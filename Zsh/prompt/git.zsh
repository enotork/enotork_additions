#!/bin/zsh

########################################################################################################################
# Define common functions and variables for ZSH prompts
#
# Defined functions:
#	precmd_hook_git		: Function to be called from ZSH's precmd hook

########################################################################################################################
### Function for precmd hook
#
# Check if the current working directory is a gir repository and get informations about it.
#
# Defined variables:
#	isGit				: 1 if we are in a git repository, 0 otherwise
#	gitBranchName		: The current active branch name
#	gitIsDirty			: 1 if the repo is dirty, 0 if it's clean
#	gitHaveUntracked	: 1 if there are untracked files in the repo, 0 otherwise
#	gitHaveRemote		: 1 if the repo have at least 1 remote, 0 otherwise
#	gitNeedPush			: 1 if the repository need a push to remote, 0 otherwise

precmd_hook_git() {
	### Check if we are in a git repo
	isGit=0
	git branch >/dev/null 2>/dev/null
	[ ! ${?} -eq 0 ] && return
	### This is a git repo
	isGit=1
	
	### Get branch name
	gitBranchName=`git rev-parse --abbrev-ref HEAD`
	
	### Check for modified files
	result=`git ls-files --modified`
	[ -n "${result}" ] && gitIsDirty=1 || gitIsDirty=0
	
	### Check for untracked file
	result=`git ls-files --others --exclude-standard`
	[ -n "${result}" ] && gitHaveUntracked=1 || gitHaveUntracked=0
	
	### Check if there are remotes
	result=`git rev-parse --abbrev-ref ${gitBranchName}@{upstream} 2>/dev/null`
	if [ -n "${result}" ]
	then
		gitHaveRemote=1
		
		### Check if repository need push
		result=`git cherry -v 2>/dev/null`
		[ -n "${result}" ] && gitNeedPush=1 || gitNeedPush=0
	else
		gitHaveRemote=0
		gitNeedPush=0
	fi
}
