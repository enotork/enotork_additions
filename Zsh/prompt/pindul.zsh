#!/bin/zsh

########################################################################################################################
### Coloring

colReset="%b%f%k"
[ ${isRoot} -eq 0 ] && colUsername="%B%F{yellow}"
[ ${isRoot} -eq 1 ] && colUsername="%B%F{red}"
colHostname="%B%F{green}"
colPathname="%B%F{blue}"
colSlashes="%B%F{white}"
( [ ${isChroot} -eq 0 ] || [ ${isSsh} -eq 0 ] ) && [ ${isRoot} -eq 0 ] && colDecor="%b%F{cyan}"
( [ ${isChroot} -eq 0 ] || [ ${isSsh} -eq 0 ] ) && [ ${isRoot} -eq 1 ] && colDecor="%b%F{magenta}"
( [ ${isChroot} -eq 1 ] || [ ${isSsh} -eq 1 ] ) && [ ${isRoot} -eq 0 ] && colDecor="%B%F{cyan}"
( [ ${isChroot} -eq 1 ] || [ ${isSsh} -eq 1 ] ) && [ ${isRoot} -eq 1 ] && colDecor="%B%F{magenta}"
colAt="%B%F{white}"
colPrompt="%B%F{white}"
colExitT="%B%F{green}"
colExitF="%B%F{red}"
colState="%B%F{yellow}"

########################################################################################################################
### Strings

### Color session string with substitution of all '@'
strSession="${colUsername}${${sessionString}//@/${colAt}@${colHostname}}"

### Exit status string with variable ZSH_RPS1PAD to be expanded by precmd hooks
### Runtime check the exit status and change the color of it
strExitStatus="%(?,${colExitT},${colExitF})%?"

### Get intermediate status from ZSH
strTabs="\${(%):-%_}"
### Remove all characters but spaces
strTabs="\${${strTabs}//[^ ]}"

########################################################################################################################
### Hook at precmd function of ZSH

precmd() {
	### Call common hook function
	precmd_hook_common
	
	### Get how much long can be the pathname
	### max size - session string - 3 decorations - 20 of available input space for typing
	local maxPwdLen=$(( ${ttyColumns} - ${#sessionString} - 23 ))
	### Check if pathname is too long
	if [ ${#pwd} -gt ${maxPwdLen} ]
	then
		### It's too long, truncate it
		pwd=${pwd: -${maxPwdLen}}
		### And replace the first 3 characters with 3x '.'
		pwd=${pwd/#???/${colSlashes}...${colPathname}}
	fi
	### Pathname string with colored slashes
	ZSH_PATH=${pwd//\//${colSlashes}\/${colPathname}}
	
	### Get padding size of the exit status
	local rps1Pads=$(( 3 - ${#exitStatus} ))
	### Padding string for the exit status, a series of ' '
	ZSH_RPS1PAD=$( printf "%${rps1Pads}s" " " )
	
	### Get padding size for the PS2 interactive prompt
	local ps2Pads=$(( 3 + ${#sessionString} + ${#pwd} ))
	ZSH_PS2PAD=$( printf "%${ps2Pads}s" " " )
}
### First execution (needed when previewing)
precmd
