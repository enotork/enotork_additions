#!/bin/zsh

########################################################################################################################
# Define common functions and variables for ZSH prompts
#
# Defined variables:
#	with8bit			: 1 if '8bit' options is given, 0 otherwise
#	withGit				: 1 if 'git' options is given, 0 otherwise
#	isRoot				: 1 if user is root, 0 otherwise
#	isChroot			: 1 if this is a chroot environment, 0 otherwise
#	isSsh				: 1 if this shell is in a ssh session, 0 otherwise
#	sessionString		: The session string, like user@host[@host]
#	escape*				: Escape characters for moving cursor, ecc...
#
# Defined functions:
#	precmd_hook_common	: Function to be called from ZSH's precmd hook

########################################################################################################################
### Check options

with8bit=0
withGit=0
while [ -n "${1}" ]
do
	case "${1}" in
		"8bit")
			with8bit=1
			;;
		"git")
			### Check if git is installed
			[ -e /usr/bin/git ] && withGit=1
			;;
	esac
	shift
done

########################################################################################################################
### User/host(s) session

### Check if user or not
! ( [ "${EUID}" = "0" ] || [ "${USER}" = "root" ] )
isRoot=${?}

### Get the real hostname from configuration files (and not from the runtime hostname command)
if [ -f /etc/hostname ]
then
	### systemd
	hostname=`cat /etc/hostname`
elif [ -f /etc/conf.d/hostname ]
then
	### OpenRC
	source /etc/conf.d/hostname
else
	### Unknown
	hostname=`hostname`
fi
### If this hostname is not the first in the ZSH_HOSTS list variable, then add it (we are in a chroot)
[[ ! "${ZSH_HOSTS}" =~ "^@${hostname}" ]] && export ZSH_HOSTS="@${hostname}${ZSH_HOSTS}"

### Count how many '@' there are inside ZSH_HOSTS, if they are more than one, we are in a chroot
[ ${#${ZSH_HOSTS//[^@]}} -eq 1 ]
isChroot=${?}

### Check if we are in a ssh session
[ -z "${SSH_CLIENT}" ] && [ -z "${SSH_TTY}" ]
isSsh=${?}

### Session string: user@host[@host[@host[...]]], optional hosts if we are in a chroot
sessionString="${(%):-%n}${ZSH_HOSTS}"

########################################################################################################################
### Other things

### Special escape characters for cursor moving
escapeUp=$'%{\e[A%}'
escapeDown=$'%{\e[B%}'
escapeRight=$'%{\e[C%}'
escapeLeft=$'%{\e[D%}'
escapeEnter=$'\n'

########################################################################################################################
### Function for precmd hook
#
# Get the correct current working directory with the user root converted in '~', and also get the exit status
# of the previous executed command and the size of the terminal
#
# Defined variables:
#	ttyColumns			: Number of columns of the console
#	pwd					: The present working dir
#	exitStatus			: Exit status of the previous executed command

precmd_hook_common() {
	### Get exit status from ZSH
	exitStatus=${(%):-%?}
	### Get console width
	ttyColumns=$(( `tput cols` - 1 ))
	### Get pathname (but not from ZSH because shows '~' in every home of every user)
	pwd=${PWD/#${HOME}/'~'}
}
