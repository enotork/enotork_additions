#!/bin/zsh

########################################################################################################################
### Coloring

colReset="%b%f%k"
[ ${isRoot} -eq 0 ] && colUsername="%B%F{yellow}"
[ ${isRoot} -eq 1 ] && colUsername="%B%F{red}"
colHostname="%B%F{green}"
colPathname="%B%F{blue}"
colSlashes="%B%F{white}"
( [ ${isChroot} -eq 0 ] || [ ${isSsh} -eq 0 ] ) && [ ${isRoot} -eq 0 ] && colDecor="%b%F{cyan}"
( [ ${isChroot} -eq 0 ] || [ ${isSsh} -eq 0 ] ) && [ ${isRoot} -eq 1 ] && colDecor="%b%F{magenta}"
( [ ${isChroot} -eq 1 ] || [ ${isSsh} -eq 1 ] ) && [ ${isRoot} -eq 0 ] && colDecor="%B%F{cyan}"
( [ ${isChroot} -eq 1 ] || [ ${isSsh} -eq 1 ] ) && [ ${isRoot} -eq 1 ] && colDecor="%B%F{magenta}"
colAt="%B%F{white}"
colExitT="%B%F{green}"
colExitF="%B%F{red}"
colState="%B%F{yellow}"
### Git plugin
colGitSymbol="%B%F{white}"
colGitBranchMaster="%B%F{green}"
colGitBranchDevelop="%B%F{yellow}"
colGitBranchOthers="%B%F{blue}"
colGitClean="%B%F{green}"
colGitDirty="%B%F{red}"
colGitUntracked="%B%F{yellow}"
colGitCloud="%B%F{cyan}"
colGitSync="%B%F{magenta}"

########################################################################################################################
### Decorations

### Standard decorations characters
barH='-'
barV='|'
cornerTL='/'
cornerBL='\\'
cornerBR='/'
cornerTR='\\'
crossL='|'
crossR='|'
### 8-bit decorations
if [ ${with8bit} -eq 1 ]
then
	### Unicode
	if [[ ${LC_ALL:-${LC_CTYPE:-$LANG}} = *UTF-8*  || ${LC_ALL:-${LC_CTYPE:-$LANG}} = *utf8* ]]
	then
		barH=$'\xe2\x94\x80'
		barV=$'\xe2\x94\x82'
		cornerTL=$'\xe2\x94\x8c'
		cornerTR=$'\xe2\x94\x90'
		cornerBL=$'\xe2\x94\x94'
		cornerBR=$'\xe2\x94\x98'
		crossL=$'\xe2\x94\x9c'
		crossR=$'\xe2\x94\xa4'
		### Git plugin
		checkMark=$'\xe2\x9c\x93'
		crossMark=$'\xe2\x9c\x95'
		plusSign=$'\x2b'
		gitSymbol=$'\xc2\xb1'
		cloudSym=$'\xe2\x9b\x85'
		upArrow=$'\xe2\x86\x91'
	else
		barH=$'\xc4'
		barV=$'\xb3'
		cornerTL=$'\xda'
		cornerTR=$'\xbf'
		cornerBL=$'\xc0'
		cornerBR=$'\xd9'
		crossL=$'\xc3'
		crossR=$'\xb4'
	fi
fi

########################################################################################################################
### Strings

### Color session string with substitution of all '@'
strSession="${colUsername}${${sessionString}//@/${colAt}@${colHostname}}"

### Exit status string with variable ZSH_RPS1PAD to be expanded by precmd hooks
### Runtime check the exit status and change the color of it
strExitStatus="%(?,${colExitT},${colExitF})%?"

### Get intermediate status from ZSH
strTabs="\${(%):-%_}"
### Remove all characters but spaces
strTabs="\${${strTabs}//[^ ]}"

########################################################################################################################
### Hook at precmd function of ZSH

precmd() {
	### Call common hook function
	precmd_hook_common
	
	### Get how much long can be the pathname (max size - session string - 10 decorations)
	local maxPwdLen=$(( ${ttyColumns} - ${#sessionString} - 10 ))
	### Get padding size at the right of the pathname (for 2-lines prompts)
	local ps1Pads=$(( ${maxPwdLen} - ${#pwd} ))
	### Check if padding right is negative (meaning too long pathname)
	if [ ${ps1Pads} -lt 0 ]
	then
		### It's too long, truncate it
		pwd=${pwd: -${maxPwdLen}}
		### And replace the first 3 characters with 3x '.'
		pwd=${pwd/#???/${colSlashes}...${colPathname}}
		### Reset right-padding
		ps1Pads=0
	fi
	### Build the padding strings with spaces
	strPS1Pad=$( printf "%${ps1Pads}s" " " )
	### Pathname string with colored slashes
	ZSH_PATH=${pwd//\//${colSlashes}\/${colPathname}}
	### Padding string for the first line, substitute every spaces with the bar character
	ZSH_PS1PAD=${${strPS1Pad}// /${barH}}
	
	### Get padding size of the exit status
	local rps1Pads=$(( 3 - ${#exitStatus} ))
	if [ ${rps1Pads} -eq 0 ]
	then
        ### No padding
        ZSH_RPS1PAD=""
	else
        ### Padding string for the exit status, a series of ' '
        ZSH_RPS1PAD=$( printf "%${rps1Pads}s" " " )
	fi
	
	### Check for git plugin
	if [ ${withGit} -eq 1 ]
	then
		### Call common git hook function
		precmd_hook_git
		
		### Think we are starting out of a repository
		ZSH_GIT=""
		ZSH_GIT_BRANCH=""
		ZSH_GIT_STATUS=""

		### Check if we are in a git repository
		if [ ${isGit} -eq 1 ]
		then
			### Show we are in a git repository
			ZSH_GIT="${colGitSymbol}${gitSymbol} "

			### Set branch name expanding variable
			ZSH_GIT_BRANCH="${colGitBranchOthers}${gitBranchName} "
			[ "${gitBranchName}" = "master" ] && ZSH_GIT_BRANCH="${colGitBranchMaster}${gitBranchName} "
			[ "${gitBranchName}" = "develop" ] && ZSH_GIT_BRANCH="${colGitBranchDevelop}${gitBranchName} "
			### Check if branch has remote
			[ ${gitHaveRemote} -eq 1 ] && ZSH_GIT_BRANCH="${colGitCloud}${cloudSym}${ZSH_GIT_BRANCH}"

			### Check if repo is dirty or clean
			[ ${gitIsDirty} -eq 1 ] \
				&& ZSH_GIT_STATUS="${colGitDirty}${crossMark}" \
				|| ZSH_GIT_STATUS="${colGitClean}${checkMark}"
			### Check if there are untracked files
			[ ${gitHaveUntracked} -eq 1 ] && ZSH_GIT_STATUS="${ZSH_GIT_STATUS}${colGitUntracked}${plusSign}"
			### Check if branch need push
			[ ${gitNeedPush} -eq 1 ] && ZSH_GIT_STATUS="${ZSH_GIT_STATUS}${colGitSync}${upArrow}"
		fi
	fi
}

### First execution (needed when previewing)
precmd
