#!/bin/zsh

### ZSH scripts pathname
ZSH_DIR=${${0:A}:h}

### Usefull only when debugging files on /home project directory
[[ ! "${ZSH_DIR}" =~ "/usr/share/zsh" ]] && fpath=(${fpath} ${ZSH_DIR}/functions)

### Startup colorations in 'ls'
if [ -f ~/.dir_colors ]
then
    eval $(dircolors -b ~/.dir_colors)
elif [ -f /etc/DIR_COLORS ]
then
    eval $(dircolors -b /etc/DIR_COLORS)
fi

### Source all components file on zsh.d directory
for z in ${ZSH_DIR}/zsh.d/*.zsh
	[ -r "${z}" ] && source "${z}"
