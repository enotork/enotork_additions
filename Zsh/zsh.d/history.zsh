#!/bin/zsh

export HISTSIZE=2000
export HISTFILE="${HOME}/.zsh_history"
export SAVEHIST=2000

### Evita duplicati di command-line duplicate nella history (cancella il più vecchio e tiene il più recente)
setopt hist_ignore_all_dups
### Evita di aggiungere command-line che iniziano con uno spazio (se si vuole evitare che un comando venga salvato)
setopt hist_ignore_space
### Salva le command-line nella history in ordine incrementale in caso di più sessioni aperte (disponibili solo alla prossima sessione)
setopt inc_append_history
### Usa la stessa history list tra varie sessioni
#setopt share_history
