#!/bin/zsh

### Cambio automatico cartella senza il comando 'cd', basta scrivere il nome della cartella
setopt autocd

### Correzione automatica dei comandi
setopt correct
### Correzione automatica dei comandi e dei parametri
#setopt correct_all

### In caso di processi eseguiti in background (con il '&' finale) mostra il suo exit status subito senza aspettare un nuovo prompt
setopt notify

### Disattiva i beep nei completamenti ambigui
setopt nobeep
