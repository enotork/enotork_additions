#!/bin/zsh

### Attiva la funzione di autocompletamento
autoload -U compinit
compinit

### Estende i caratteri '#', '~' e '^' come pattern nella generazione dei filenames
#setopt extended_glob

### In caso di autocompletamenti a metà parola, dopo essere completato porta a fine parola
###  Esempio:   ls /etc/c|/d
###  Premere TAB supponendo che il cursore sia '|' per completare la cartella che inizia con 'c'
setopt always_to_end

### Attiva il menu di selezionamento (con i tasti freccia)
zstyle ':completion:*' menu select

# Mostra l'autocompletamento case-insensitive, esempio ls /etc/d<TAB> mostra anche DIR_COLORS
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

### Ordine completamenti: prima espansione, poi completamento, ecc....
zstyle ':completion:*' completer _expand _complete _ignored _approximate

### In caso di lista di selezione molto lunga, viene mostrato questo testo per indicare a che punto ci si trova della lista
zstyle ':completion:*' select-prompt '%SPosizione: %p%s'

### Cache al completamento
zstyle ':completion::complete:*' use-cache 1

### Rehash per programmi appena installati e non ancora disponibili per il completamento
zstyle ':completion:*' rehash true

### Ignora i completamenti per i comandi che non abbiamo
zstyle ':completion:*:functions' ignored-patterns '_*'

################################################################################
### Colorazione ZSH
# %U, %u attiva o disattiva l'underline
# %B, %b attiva o disattiva il boldness
# %F, %f attiva o disattiva la colorazione di foreground
# %K, %k attiva o disattiva la colorazione di background
# %d descrizione del parametro

### Mostra la descrizione come titolo sulla lista
zstyle ':completion:*:descriptions' format '%U%B%F{yellow}%d%f%b%u'

### Mostra i completamenti in gruppi
zstyle ':completion:*' group-name ''

### Messaggio in caso di matching mancante
zstyle ':completion:*:warnings' format '%B%F{yellow}Nessun match per: %f%b%d'

################################################################################
### Colorazione list-colors
# Tutto il testo viene colorato di un colore tranne i pattern che compaiono
# tra parentesi tonde. La sintassi è simile a quella delle regexp ma con
# il # al posto del *.
#
# Esempio:         "=(#b) #([0-9]#) #([0-9]#.[0-9])*=36=31=32"
#                   \__/ \/\_____/ \/\___________/ |\_/\_/\_/
#                    |   |    |    |       |       | |  |  |
#   Preambolo -------'   |    |    |       |       | |  |  |
#      Spazi ------------'    |    |       |       | |  |  |
#         Primo pattern ------'    |       |       | |  |  |
#            Spazi ----------------'       |       | |  |  |
#               Secondo pattern -----------'       | |  |  |
#                  Tutto il resto -----------------' |  |  |
#                     Colore di tutto il testo ------'  |  |
#                        Colore del primo pattern ------'  |
#                           Colore del secondo pattern ----'

### Lista di completamento colorata
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
### Personalizza i colori dei vari gruppi di completamento
zstyle ':completion:*:commands' list-colors '=*=1;32'
zstyle ':completion:*:builtins' list-colors '=*=1;31'
zstyle ':completion:*:functions' list-colors '=*=1;35'
zstyle ':completion:*:aliases' list-colors '=*=1;33'
zstyle ':completion:*:suffix-aliases' list-colors '=*=4;33'
zstyle ':completion:*:reserved-words' list-colors '=*=33'
zstyle ':completion:*:parameters' list-colors '=*=36'
zstyle ':completion:*:local-directories' list-colors '=*=1;34'
### Colora le opzioni nei parametri
zstyle ':completion:*:options' list-colors '=^(-- *)=1;32'

################################################################################
### Specifico per comandi

### Cambia il comando per la lista processi (kill)
zstyle ':completion:*:processes' command 'ps -au ${USER} o pid,%cpu,comm --sort -%cpu'
### Cambia i colori della lista processi per kill e killall
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) #([0-9]#.[0-9])*=36=32=33'
zstyle ':completion:*:*:killall:*:processes-names' list-colors '=(#b)*=36'
### Mostra subito il menu in kill
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always

### Il comando cd ignora la directory superiore quando si va indietro (es. cd ../<TAB>)
zstyle ':completion:*:cd:*' ignored-patterns '(*/)#lost+found'
zstyle ':completion:*:(cd|mv|cp):*' ignore-parents parent pwd

### Nei comandi rm kill e diff ignora il ripetere lo stesso file 2 o piu' volte
zstyle ':completion:*:(rm|kill|diff):*' ignore-line yes
