#!/bin/zsh

bindkey "[2~" overwrite-mode
bindkey "[3~" delete-char

if [[ ${TERM} == linux ]]
then
	bindkey "[1~" beginning-of-line
	bindkey "[4~" end-of-line
elif [[ ${TERM} == xterm ]]
then
	bindkey "[H" beginning-of-line
	bindkey "[F" end-of-line
elif [[ ${TERM} == xterm-256color ]]
then
	bindkey "[H" beginning-of-line
	bindkey "[F" end-of-line
elif [[ ${TERM} =~ rxvt* ]]
then
	bindkey "[7~" beginning-of-line
	bindkey "[8~" end-of-line
fi

bindkey "[5~" beginning-of-history
bindkey "[6~" end-of-history
