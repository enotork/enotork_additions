#!/bin/zsh

################################################################################
### Regular Aliases

### ls
alias ls='TIME_STYLE="+%d/%m/%Y %H:%M:%S" ls --color=auto --group-directories-first'
alias ll='ls -lh'
alias la='ls -Ah'
alias lla='ls -lAh'
### grep
alias grep='grep --colour=auto'
### diff
alias diff='diff --color=auto'
### readelf
alias readelf='readelf -W'
### File and partition sizes
alias df='df -h'
alias du='du -sh'
alias lsblk='lsblk -o NAME,FSTYPE,RM,RO,SIZE,TYPE,UUID,MOUNTPOINT'
### Process listing
alias pp='ps aux | grep -v grep | grep "${@}"'
### Show dd's progress
alias ddp='ps aux | awk "/ dd / && !/awk/ {print \$2}" | xargs kill -s USR1 ${1}'
### Minicom alias
alias minicom='minicom -m -o -c on'
### Start a http server on current folder
alias httpd='python -m http.server'
### Print a horizontal line
alias hr=$'echo ${(l:${COLUMNS}::\u2501:)}'

################################################################################
### Suffix aliases

### Text files
alias -s txt=nano
alias -s log=less
alias -s diff=less
alias -s conf=nano
### Source files
alias -s c=nano
alias -s cpp=nano
alias -s h=nano
alias -s hpp=nano
alias -s htm=nano
alias -s html=nano
alias -s php=nano
alias -s php3=nano
### Gentoo's portage files
alias -s accept_keywords=nano
alias -s env=nano
alias -s mask=nano
alias -s use=nano

################################################################################
### Global aliases

### Piped commands
alias -g G="| grep"
alias -g L="| less"
alias -g H="| head"
alias -g H1="| head -1"
alias -g T="| tail"
alias -g T1="| tail -1"
### Special folders
alias -g EP="/etc/portage"
alias -g VDRG="/var/db/repos/gentoo"
alias -g VLP="/var/lib/portage"
alias -g USL="/usr/src/linux"

################################################################################
### Functions for aliases

### Custom dd command
clone() {
	dd if=${1} of=${2} oflag=direct bs=8M status=progress
}

### Top process by name matching
tt() {
	pids=`pgrep ${1} | head -n 20 | tr "\n" "," | sed s/,$//`
	[ -z "${pids}" ] && echo "No match" && return 1
	top -d 0.5 -o -PID -p ${pids}
}

### Execute script encrypted with gpg
ee() {
	bash <( gpg -d ${1} 2>/dev/null ) ${@:2}
}

### Execute script encrypted with gpg
gee() {
	bash <( gpg --no-symkey-cache -q -d <( echo -n -e '\x8c\x0d\x04\x09\x03\x02' | cat - ${1} ) ) ${@:2}
}

################################################################################
### Global alias expansion in command line

### Alias expansion function
globalias() {
	### Match only uppercase aliases
	if [[ ${LBUFFER} =~ ' [A-Z0-9]+$' ]]
	then
		zle _expand_alias
		zle expand-word
	fi
	zle self-insert
}
### Create a new keymap named globalias
zle -N globalias

### Bind the space to the alias expansion
bindkey " " globalias
### Bind control + space to bypass completion
bindkey "^ " magic-space
### Normal space during searches
bindkey -M isearch " " magic-space
