#!/bin/zsh

### Attiva la funzione 'prompt' per cambiare tema del prompt al volo
autoload -U promptinit
promptinit

### Permette le sostituzioni di variabili ${...} nel prompt
setopt promptsubst
