########################################################################################################################
### Zsh
#
# Install ZSH components.
#
########################################################################################################################
if(NOT DEFINED ZSH)
	return()
endif()

install(FILES
	"functions/_nfschroot"
	"functions/_sshlpf"
	"functions/prompt_pindul_setup"
	"functions/prompt_neon_setup"
	DESTINATION "${CMAKE_INSTALL_DATADIR}/zsh/site-functions")
install(FILES
	"zsh-enotork.zsh"
	DESTINATION "${CMAKE_INSTALL_DATADIR}/zsh/site-contrib/zsh-enotork")
install(DIRECTORY
	"prompt"
	"zsh.d"
	DESTINATION "${CMAKE_INSTALL_DATADIR}/zsh/site-contrib/zsh-enotork")
