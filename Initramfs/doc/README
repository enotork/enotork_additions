################################################################################
##### Command line examples:


BOOTIF=01-aa-bb-cc-dd-ee-ff
	Hardware address dash-separated of the boot interface in a PXE environment.
	Where aa-bb-cc-dd-ee-ff is the network's interface MAC address and 01 is
	the ARP type (01 is for Ethernet).

	This is usefull when the system has multiple network interface and we want
	to configure and use the same interface for booting and NFS mounting.


init=...
	Specify the init file to launch after the one inside the initrd has done all
	it's jobs.


ip=<ipaddr>:<servaddr>[:<gateway>[:<netmask>[:<hostname>[:<iface>[:<autoconf>
		[:<dns0>[:<dns1>[:<ntp>]]]]]]]]
	Configure IP address for devices, routing, hostname and DNS address.

	<ipaddr>	The IP address to assign the default interface or <iface>
				if specified.

	<servaddr>	The remote server IP address, used for NFS mounts.

	<gateway>	Gateway address if the server is on a different subnet.

	<netmask>	Netmask for local network. If unspecified the netmask is
				derived from the client IP address assuming classful
				addressing.

	<hostname>	Name to assign to the client.

	<iface>		Network device to use and to configure with the above
				parameters.

	<autoconf>	Method to use with autoconfiguration:
				off or none		: Do not autoconfigure.
				on or any		: Use any protocol available in the kernel.
				dhcp			: Use DHCP.
				### TODO ###
				bootp			: Use BOOTP.
				rarp			: Use RARP.
				both			: Use BOOTP and RARP but not DHCP.
				### TODO ###

	<dns0>		Set the first DNS server address.

	<dns1>		Set the second DNS server address.

	<ntp>		Set the NTP address for time synchronization.


keymap=en
	Load the specified keymap file for the keyboard used on console, but not on
	X11 environment.

### TODO ###
modulesroot=...
	Specify the root filesystem where kernel modules are present. This parameter
	can take the same values of "root=" parameter.
### TODO ###


nfsroot=[<nfs_server>:]<nfs_root>[,<nfs_options>]
	Specify the NFS share of the root filesystem.
	### TODO ###
	If the "nfsroot=" parameter is NOT given on the command line, then the default
	"/tftpboot/%s" will be used.
	### TODO ###

	<nfs_server>	IP address or hostname of the NFS server. If not specified
					the default is used from <serv> on "ip=" parameter.

	<nfs_root>		Pathname of the directory on the server to mount.
					### TODO ###
					If there is a "%s" token in the string, it will be replaced
					by the ASCII-representation of the client's IP address.
					### TODO ###

	<nfs_options>	Commas separated NFS options to append to the mount command.


ro
	Mount the root filesystem as read-only.


root=...
	Specify the root destination filesystem.
	Possible vaues are:

	/dev/sda1
		Root filesystem is on block device with kernel name sda1. This configuration can have some issues
		when hard disk connection are changed between reboots and the root fs can later be named ex. sdb1.

	LABEL=
		Search for root filesystem on block devices using fs label.

	UUID=
		Search for root filesystem on block devices using fs UUID.

	PARTUUID=
		Search for root filesystem on block devices using partition table's UUID.

	PARTLABEL=
		Search for root filesystem on block devices using partition table's label.

	/dev/nfs
		Root filesystem is on a NFS share. This require the 'nfsroot=' parameter to specified.





Root on hard disk (mounted read-only)
	root.type=dev
	root.location=/dev/sda2
	root.options=ro

Root on a NFS share
	root.type=nfs
	root.location=192.168.1.25:/srv/nfs

Root on a image file inside a device with specified UUID
	root.type=loop:dev
	root.location=UUID=.......
	root.file=image.img

Root on a image file inside NFS share
	root.type=loop:nfs
	root.location=192.168.1.25:/srv/nfs
	root.file=image.img
	root.fstype=ext4
	root.delay=10

Root on a image file inside a device with specified LABEL
	root.type=loop:dev
	root.location=LABEL=.......
	root.file=image.squashfs
	root.fstype=squashfs

Root on a squashfs file inside NFS share
	root.type=loop:nfs
	root.location=192.168.1.25:/srv/nfs
	root.file=image.squashfs
	root.fstype=squashfs

Root on a squashfs file downloaded via http and filename from url
	root.type=loop:http
	root.location=http://server.local/image.squashfs
	root.fstype=squashfs

Root on a squashfs file downloaded via http and custom filename
	root.type=loop:http
	root.location=http://server.local/autodownloaded_dynamic.php
	root.file=image.squashfs
	root.fstype=squashfs

