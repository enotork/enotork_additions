########################################################################################################################
### Zenstates
#
# Install Zenstates utility.
#
########################################################################################################################
if(NOT DEFINED ZENSTATES)
	return()
endif()

install(PROGRAMS "zenstates_disable_c6" DESTINATION ${CMAKE_INSTALL_SBINDIR})
install(PROGRAMS "zenstates.py" DESTINATION "${CMAKE_INSTALL_DATADIR}/${PROJECT_NAME_L}/zenstates")
install(FILES "zenstates.service" DESTINATION ${SYSTEMD_UNIT_DIR})
