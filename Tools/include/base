#!/bin/bash

################################################################################
### Version 1.5
###   ++ Fixed HOSTNAME variable for systemd compatibility
### Version 1.4
###   ++ Added function fatal
###   -- Deprecated die in favour of fatal
###   ++ Updated check root to use the new fatal function
###   ++ New function version to print styled program version
###   ++ Some restyling in echo functions
### Version 1.3
###   ++ Added getKernelSourceVersion
### Version 1.2
###   ++ Improoved HOSTNAME

### Name of script running
BASENAME=`basename $0`

### Get hostname on gentoo hosts
if [ -f /etc/conf.d/hostname ]
then
	HOSTNAME=`sed -rn "/hostname/ s/.*=\"?(.*[^\"])\"?/\1/p" /etc/conf.d/hostname`
elif [ -f /etc/hostname ]
then
	HOSTNAME=`cat /etc/hostname`
else
	HOSTNAME=""
fi

### Get running kernel version
KV=`uname -r`

################################################################################
### Print standard welcome message
welcome(){
	echo -e "\n${BLUE} ***** ${YELLOW}${BASENAME} ${CYAN}${VERSION}${BLUE} ***** ${NORMAL}\n"
}

################################################################################
### Print standard version message and exit
version(){
	echo -e " ${YELLOW}${BASENAME} ${CYAN}${VERSION}${NORMAL}"
	exit 0
}

################################################################################
### Fatal error function
# ${1} = Exit status
# ${2} = Error message (same for ${4},${6},${8},...)
# ${3} = Error detail (same for ${5},${7},${9},...)
fatal(){
	### Save exit status
	status=${1}
	shift 1
	echo -en " ${RED}ERROR:"

	### Loop for others parameters
	color=${NORMAL}
	while [ ${#} -gt 0 ]
	do
		echo -en " ${color}${1}"
		shift 1
		[ ${color} = ${NORMAL} ] \
			&& color=${CYAN} \
			|| color=${NORMAL}
	done

	### End
	echo
	exit ${status}
} >&2

################################################################################
### DEPRECATED in favor of "fatal" function
# ${1} : Error to print to stderr
# ${2} : Exit status
die(){
	fatal ${2} ${1}
}

################################################################################
### Controlla se si è root all'esecuzione del programma
check_root(){
	[ ${UID} -ne 0 ] && fatal 1 "Access denied, you are not root !"
}

################################################################################
### Get kernel version from sources in /usr/src/linux
getKernelSourceVersion(){
	cd /usr/src/linux
	make kernelrelease 2>/dev/null
}
