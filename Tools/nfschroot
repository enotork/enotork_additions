#!/bin/bash

source /usr/share/enotork_additions/include/base
source /usr/share/enotork_additions/include/colors

VERSION="3.4"

################################################################################
# Mount a remote root filesystem over NFS to a local folder, chroot into them
# and use this environment to install packages using local cpu power
#
# Version 3.4:
#	Trying to fix mountings
#	++ Added support for remote pathname mount as optional second argument
# Version 3.3:
#	Include this script in enotork_additions
#	-- Removed make.conf update for MAKEOPTS variable, use build.env file instead
#	++ Updated pathnames on unmounting
#	++ Added /tmp/portage and /var/tmp/portage support
#	-- Removed CHROOTS variable
#	++ Changed BASE_DIR to /mnt/nfs because /run/nfschroot was binded
# Version 3.2:
#	++ Fixed NFS mount options
#	++ Fixed check of filesystem usage
#	-- Removed starting of rpc.statd
#	-- Removed /var/tmp/portage useless creation
#	++ Fixed chroot commands, only on first chroot execute the "env-update ; source /etc/profile"
# Version 3.1:
#	++ Fixed error on missing remote /var/tmp/portage
#	++ Fixed deleting remote MAKEOPTS flag on logout when missing a commented MAKEOPTS (for some strange reasons)
# Version 3.0:
#	Rebuilded version
#	++ Using volatile base folder on /run/nfschroot instead of /mnt/nfschroot
#	++ Allow more than one sessions, identified by subfolders of /run/nfschroot
#	++ Race conditions beyond processes driven by 'fuser' instead of temp-files
#	++ Remote /var/tmp/portage is now a subfolder of local /var/tmp/portage
#	++ Change remote MAKEOPTS variable to local value, and revert back on exit
#	++ Mount a 32bit environment (i686) on 64bit environment
#	++ Fixed remote shell variable to load the correct shell at chroot
# Version 2.0:
#

BASE_DIR="/mnt/nfs"
MAKE_CONF="/etc/portage/make.conf"

################################################################################
### Starting

### Checking parameters
if [ ${#} -lt 1 -o ${#} -gt 2 ]
then
	echo -e "${WHITE}Usage: ${YELLOW}${BASENAME} ${WHITE}< ${CYAN}hostname${WHITE} | ${CYAN}ip${WHITE} >${NORMAL} ${WHITE}[ ${CYAN}path${WHITE} ]${NORMAL}"
	exit 1
fi
### Get hostname
host=${1}
### Get pathname
path=${2}
### Remove leading slash
path=${path#/}
### Remove trailing slash
path=${path%/}

### Checking root
welcome
check_root

################################################################################
### Struct mount

### Check if not already mounted structure
dir=${BASE_DIR}/${host}
if [ -z "`mount | grep ${dir}`" ]
then
	### Creating directories
	pExec "mkdir -p ${dir}" "Creating base directory ${dir}"
	### Build up structure
	pMsg "Mounting ${host} chroot structure"
	### Remote root filesystem
	pExecE "mount ${host}:/${path} ${dir} -o nolock,rsize=32768,wsize=32768" "/" 1
	### Pseudo filesystems
	pExecE "mount -t proc /proc ${dir}/proc" "/proc" 1
	pExecE "mount --rbind /sys ${dir}/sys" "/sys" 1
	mount --make-rslave ${dir}/sys
	pExecE "mount --rbind /dev ${dir}/dev" "/dev" 1
	mount --make-rslave ${dir}/dev
#	pExecE "mount -t devpts none ${dir}/dev/pts -o nosuid,noexec,gid=5,mode=620" "/dev/pts" 1
#	pExecE "mount -t tmpfs none ${dir}/dev/shm -o nodev,nosuid,noexec" "/dev/shm" 1
#	pExecE "mount --rbind /run ${dir}/run" "/run" 1
	### Portage's dirs
	pExecE "mount --bind /var/db/repos/gentoo ${dir}/var/db/repos/gentoo" "/var/db/repos/gentoo" 1
	pExecE "mount --bind /var/cache/distfiles ${dir}/var/cache/distfiles" "/var/cache/distfiles" 1
	mkdir -p /tmp/portage/${host} ${dir}/tmp/portage/${host}
	pExecE "mount --bind /tmp/portage/${host} ${dir}/tmp/portage" "/tmp/portage" 1
	mkdir -p /var/tmp/portage/${host} ${dir}/var/tmp/portage/${host}
	pExecE "mount --bind /var/tmp/portage/${host} ${dir}/var/tmp/portage" "/var/tmp/portage" 1
	### Build environment for portage
	pExecE "mount --bind /etc/portage/package.env/build.env ${dir}/etc/portage/package.env/build.env" "/etc/portage/package.env/build.env" 1
	### Update remote environment on chroot
	envcmd="env-update >/dev/null ; source /etc/profile ;"
fi

################################################################################
### Chrooting

### Extracting local and remote CHOST
lchost=`sed -rn "/CHOST/ s/.*=\"(.*)-pc-linux-gnu\"/\1/p" ${MAKE_CONF}`
rchost=`sed -rn "/CHOST/ s/.*=\"(.*)-pc-linux-gnu\"/\1/p" ${dir}${MAKE_CONF}`
[ "${lchost}" == "x86_64" -a "${rchost}" == "i686" ] && prepend="linux32"
### Extracting remote root shell
shell=`grep root:x:0:0:root ${dir}/etc/passwd | cut -d ':' -f 7`

### Exec chrooting
pMsg "Entering ${host} chroot environment"
pSeparator "*" ${YELLOW} 0 0 1 1

${prepend} chroot ${dir} /bin/bash -c "${envcmd} exec ${shell}"

pSeparator "*" ${YELLOW} 0 0 1 1
pMsg "Leaving ${host} chroot environment"

################################################################################
### Struct umount

### Check for running nfschroot processes
lsof ${dir} 2>&1 >/dev/null
if [ ${?} -eq 1 ]
then
	### Cleaning temporary folders
	rm -rf /tmp/portage/${host} ${dir}/tmp/portage/${host}
	rm -rf /var/tmp/portage/${host} ${dir}/var/tmp/portage/${host}
	### Dismantling the structure
	pMsg "Unmounting ${host} chroot structure"
	for m in `cat /proc/mounts | awk '{print $2}' | grep "^${dir}" | sort -r`
	do
		name=${m#${dir}}
		[ -z ${name} ] && name="/"
		[ -n "cat /proc/mounts | awk '{print $2}' | grep '^${m}$'" ] \
			&& pExec "umount -l ${m}" "${name}" 1 \
			|| pHidd "${m}" 1
	done
	rmdir ${dir}
fi
